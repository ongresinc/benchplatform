#!/bin/bash

sudo add-apt-repository main
sudo add-apt-repository universe
sudo add-apt-repository restricted

# ZFS
sudo apt-add-repository multiverse

# Mongo
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list


## Installing

for attempt in $(seq 1 3)
do
    sudo apt-get update -yq --fix-missing && \
    sudo apt-get install -yq mongodb-org && \
    sudo apt-get install -yq zfsutils-linux && \
    sudo apt-get install -yq python3 python3-pip && \
    sudo apt-get install -yq ec2-api-tools && \
    sudo apt-get install -yq jq && \
    sudo apt-get install -yq sysstat &&\
    sudo apt-get install -yq awscli
done

# cd /opt
# sudo curl -O https://bootstrap.pypa.io/get-pip.py
# sudo python3 get-pip.py --user
# pip3 install --upgrade pip
# pip3 install awscli --upgrade --user
