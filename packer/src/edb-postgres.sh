#!/bin/bash

# Setup the repository (edit the username/password as appropriate)
sudo sh -c 'echo "deb https://gerardo.herzig:vZfk9GfVX6kCGmHy@apt.enterprisedb.com/$(lsb_release -cs)-edb/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/edb-$(lsb_release -cs).list'

# Add support for secure APT repositories
sudo apt-get -y install apt-transport-https

# Add the EDB signing key
wget -q -O - https://gerardo.herzig:vZfk9GfVX6kCGmHy@apt.enterprisedb.com/edb-deb.gpg.key  | sudo apt-key add -

# Update the repository meta data
sudo apt-get update

# Install EPAS 11
sudo apt-get -y install edb-as11


