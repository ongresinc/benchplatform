#!/bin/bash

sudo add-apt-repository main
sudo add-apt-repository universe
sudo add-apt-repository restricted
# ZFS
sudo apt-add-repository multiverse

# Postgres
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main" > /etc/apt/sources.list.d/PostgreSQL.list' 

for attempt in $(seq 1 3)
do
    sudo apt-get update -yq --fix-missing && \
    sudo apt-get install -yq nano vim htop ec2-api-tools && \
    sudo apt-get install -yq python3 && \
    sudo apt install -yq python3-pip && \
    sudo apt install -yq  jq && \
    sudo apt-get install -yq postgresql-11 pgbouncer postgresql-client-11 && \
    sudo apt-get install -yq zfsutils-linux && \
    sudo  apt-get install -yq awscli
    sudo apt-get install -yq sysstat
done
