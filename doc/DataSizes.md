
## Sysbench Datadir Sizes 

```
make output | grep -A1 "sysbench.*client" | grep "[0-9]" | xargs -I {} bin/jump {} " cat /var/results/meta* | grep target ; cat /var/results/*df* | grep data ; echo " 

  "target": "mongo-sysbench-xfs-fit",
/dev/nvme0n1     40G  2.5G   38G   7% /opt/data
/dev/nvme0n1     40G  4.2G   36G  11% /opt/data

  "target": "mongo-sysbench-xfs-xl",
/dev/nvme0n1    2.5T  2.0T  527G  79% /opt/data
/dev/nvme0n1    2.5T  2.0T  488G  81% /opt/data

  "target": "mongo-sysbench-zfs-fit",
datadir         193G  1.9G  191G   1% /opt/data
datadir         193G  4.1G  189G   3% /opt/data

  "target": "mongo-sysbench-zfs-xl",
datadir         2.4T  1.8T  623G  75% /opt/data
datadir         2.4T  1.8T  591G  76% /opt/data

  "target": "pgbouncer-sysbench-xfs-fit",
/dev/nvme0n1     40G  6.1G   34G  16% /opt/data
/dev/nvme0n1     40G  4.5G   36G  12% /opt/data

  "target": "pgbouncer-sysbench-xfs-xl",
/dev/nvme0n1    3.5T  2.5T  970G  73% /opt/data
/dev/nvme0n1    3.5T  2.5T  970G  73% /opt/data

  "target": "pgbouncer-sysbench-zfs-fit",
datadir          39G  4.5G   35G  12% /opt/data
datadir          39G  3.3G   36G   9% /opt/data

  "target": "pgbouncer-sysbench-zfs-xl",
datadir         2.4T  2.1T  320G  87% /opt/data
datadir         2.4T  2.1T  321G  87% /opt/data

  "target": "postgres-sysbench-xfs-fit",
/dev/nvme0n1     40G  6.1G   34G  16% /opt/data
/dev/nvme0n1     40G  4.5G   36G  12% /opt/data

  "target": "postgres-sysbench-xfs-xl",
/dev/nvme0n1    3.5T  2.5T  970G  73% /opt/data
/dev/nvme0n1    3.5T  2.5T  970G  73% /opt/data

  "target": "postgres-sysbench-zfs-fit",
datadir          39G  4.5G   35G  12% /opt/data
datadir          39G  3.4G   36G   9% /opt/data

  "target": "postgres-sysbench-zfs-xl",
datadir         2.4T  2.1T  320G  87% /opt/data
datadir         2.4T  2.1T  321G  87% /opt/data
```

## YCSB 

