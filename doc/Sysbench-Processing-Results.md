# Processing Sysbench results from a list of `resultsUrl`



```
awk -F '=' '{print $2}' doc/bench_finales | awk -F '/' '{print $4"/"$5}' | xargs -I {} bin/getResults {}
awk -F '=' '/sysbench/ {print $2}' doc/bench_finales  | awk -F '/' '{print $4" "$5}' | xargs -I {}  bin/processSysbenchResults {} 
```


```
cat doc/sysbench_final_benchs | cut -d "=" -f2 | sed 's|s3://benchplatform-results.s3.amazonaws.com/||' | xargs -I {} bin/getResults {}
awk -F '=' '/sysbench/ {print $2}' doc/sysbench_final_benchs  | awk -F '/' '{print $4" "$5}' | xargs -I {}  bin/processSysbenchResults {} 
```

```
 mongo-sysbench-xfs-fit/bc0abe95-3652-f7b9-a829-cb7c4a6b4304
 mongo-sysbench-xfs-xl/8a32b055-1220-ff7b-be8f-07ddd03c8315
 mongo-sysbench-zfs-fit/be4af4dd-bbe1-0ca5-aeb4-13118611b3ba
 mongo-sysbench-zfs-xl/8c8c92e2-199c-0f21-d529-7600ab56f07d
 pgbouncer-sysbench-xfs-fit/4ca4ab12-2ab3-9407-2495-efb0aa13835e
 pgbouncer-sysbench-xfs-xl/4ff865c6-06a1-9508-65e6-f30891e64124
 pgbouncer-sysbench-zfs-fit/f1f7b29a-ebe8-1997-a00c-8df20b848752
 pgbouncer-sysbench-zfs-xl/a3e684aa-1308-ab49-b78b-749e2e679006
 postgres-sysbench-xfs-fit/874c34e2-a81f-6e02-4dc5-6921faf8881d
 postgres-sysbench-xfs-xl/50280e3d-52f6-3dc5-7cfe-73ac767b0f89
 postgres-sysbench-zfs-fit/e1e8fa66-d6aa-42b1-ef30-62c0928cec27
 postgres-sysbench-zfs-xl/4a4691b4-2ffb-db97-29fe-260c97349512
 ```

pgbouncer with 50 pool and max_client_conn
 ```
 pgbouncer-sysbench-xfs-fit/7f8a41cf-e0d3-adba-329b-81137c8ff47f
 pgbouncer-sysbench-xfs-xl/c9aa62a0-e2f8-102a-0fa2-982614815278
 pgbouncer-sysbench-zfs-fit/0d0e4526-dfff-d5e8-f114-944c2bf52405
 pgbouncer-sysbench-zfs-xl/4c10f5b8-e696-32ac-e587-4dd4d4ae47e6
 ```