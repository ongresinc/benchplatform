#!/bin/bash

source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"

cp -f /home/ubuntu/conf/mongo/mongod.conf /etc/mongod.conf
cp /home/ubuntu/conf/mongo/mongod.conf ${RESULTS_DIR}
# Initialize data directory?

# start mongo over 27017

chown -R mongodb: /opt/data

echo "MongoDB starting UP"
service mongod ${1} 


echo "MongoDB started"