#!/usr/bin/env bash

source /usr/local/bin/default.sh


dbname=$(getTag Dbname)
dbhost=$(getDbip)
dbport=$(getTag Dbport)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
dbengine=$(getTag Dbengine)
RESULTS_DIR="$(getTag Resultsdir)"


## Clone repo

cd /opt 
git clone https://gitlab.com/ongresinc/txbenchmark.git



cd txbenchmark
mvn clean package

## Generate SQL 
mkdir resources
wget -P resources http://www.lsv.fr/~sirangel/teaching/dataset/aircrafts.txt
wget -P resources http://www.lsv.fr/~sirangel/teaching/dataset/schedule.txt

cat resources/aircrafts.txt | sed 's/;/,/g' | sed 's/\\N//g' | mongoimport -h $dbhost -d $dbname -c aircraft --drop --type csv --fields name,icao,iata,capacity,country --ignoreBlanks
cat resources/schedule.txt | sed 's/;/,/g' | sed 's/\\N//g' | mongoimport -h $dbhost -d $dbname -c schedule --drop --type csv --fields from_airport,to_airport,valid_from,valid_until,days,departure,arrival,flight,aircraft,duration --ignoreBlanks

## We create DDL before execution
mongo --host ${dbhost} ${dbname} <<EOF
   db.createCollection("audit")
   db.createCollection("payment")
   db.createCollection("seat")
EOF

for thread_iter in $(seq  ${threads} -${step} 1 | head -${cycles} )
do
  echo "engine:$dbengine parallel:$thread_iter host:$dbhost port:$dbport duration:$duration"
  java -jar cli/target/benchmark-1.3.jar \
               --benchmark-target mongo \
               --parallelism $thread_iter \
               --day-range 1 \
               --booking-sleep 0 \
               --target-database-host $dbhost \
               --target-database-name $dbname \
               --target-database-user ""\
               --target-database-password "" \
               --target-database-port 27017\
               --metrics "PT1S" \
               --metrics-reporter csv --duration "PT${duration}S" \
               --max-connections 700
    
  ## After execution, we need to clean some data to avoid errors
    mongo --host ${dbhost} ${dbname} <<EOF
    db.audit.remove({})
    db.payment.remove({})
    db.seat.remove({})
EOF
    mv iterations.csv "${RESULTS_DIR}/iterations-${thread_iter}.csv"
    mv response-time.csv "${RESULTS_DIR}/response-time-${thread_iter}.csv"
    mv retry.csv "${RESULTS_DIR}/retry-${thread_iter}.csv"
done