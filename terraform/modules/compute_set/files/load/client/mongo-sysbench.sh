#!/usr/bin/env bash

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
RESULTS_DIR="$(getTag Resultsdir)"

sysbench /opt/sysbench-mongodb-lua/oltp-mongo.lua  \
    --tables=${tables}  \
    --threads=${threads} \
    --table-size=${numrows} \
    --mongodb-db=${dbname} \
    --mongodb-host=${dbhost} \
    --mongodb-port=27017 \
    prepare > ${RESULTS_DIR}/raw.out 

#     --rand-type=pareto \

# Stop engine after done
remote-datanode-exec "sudo service mongod stop"
sleep 20
# Do snapshot, use makeSnapshot() 
makeSnapshot

# Add some kind of check until snapshot is Status:completed

