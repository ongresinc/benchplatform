#!/usr/bin/env bash

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
RESULTS_DIR="$(getTag Resultsdir)"
sshuser=$(getTag Sshuser)
tool=$(getTag Tool)
loadsize=$(getTag Loadsize)

#wget -Oq /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar https://jdbc.postgresql.org/download/postgresql-42.2.5.jar -o /dev/null

/usr/bin/psql -h ${dbhost} -Upostgres -c "CREATE DATABASE ${dbname}"

/usr/bin/psql -h ${dbhost} -Upostgres ${dbname} <<EOF
CREATE TABLE usertable (
	YCSB_KEY VARCHAR(255) PRIMARY KEY,
	FIELD0 TEXT, FIELD1 TEXT,
	FIELD2 TEXT, FIELD3 TEXT,
	FIELD4 TEXT, FIELD5 TEXT,
	FIELD6 TEXT, FIELD7 TEXT,
	FIELD8 TEXT, FIELD9 TEXT
);
EOF


/opt/ycsb-0.15.0/bin/ycsb load  jdbc  \
	-p "db.url=jdbc:postgresql://${dbhost}:5432/${dbname}?user=postgres&reWriteBatchedInserts=true&batchupdateapi=true" \
	-cp /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar \
	-p threadcount=${threads} \
	-p recordcount=${numrows} \
	-p "workload=com.yahoo.ycsb.workloads.CoreWorkload" > ${RESULTS_DIR}/raw.out 


# This is for reducing the restore time
/usr/bin/psql -h ${dbhost} -Upostgres -c "CHECKPOINT" ${dbname}

# Stop engine after done
remote-datanode-exec "sudo service postgresql stop"
sleep 10

# Do snapshot, use makeSnapshot() 
makeSnapshot

# Add some kind of check until snapshot is Status:completed

