#!/usr/bin/env bash

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
RESULTS_DIR="$(getTag Resultsdir)"

# cp /home/${sshuser}/conf/${tool}/workloads/*${loadsize} /opt/ycsb-0.15.0/workloads/


/opt/ycsb-0.15.0/bin/ycsb load mongodb \
    -s -threads ${threads} \
    -p recordcount=${numrows}  \
    -p mongodb.url=mongodb://${dbhost}:27017/${dbname} \
    -p "workload=com.yahoo.ycsb.workloads.CoreWorkload" > ${RESULTS_DIR}/raw.out 


# Stop engine after done
remote-datanode-exec "sudo service mongod stop"
sleep 20
# Do snapshot, use makeSnapshot() 
makeSnapshot


# Add some kind of check until snapshot is Status:completed

