#!/usr/bin/env bash
source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"

device_name=$(getTag Devicename) 

ec2-create-tags $(getVolumeId) -t Name=$(getTag Target)

startSadc
startIostat
