resource "aws_key_pair" "benchplatform" {
  key_name = "${var.project}"
  public_key = "${file("~/.ssh/id_rsa_${var.project}.pub")}"
}