

# output "test_yaml" {
#     value= "${data.yaml_map_of_strings.config.output["snapshot-postgres-sysbench-xfs-fit"]}"   
# }


output "key_name" {
    value = "${aws_key_pair.benchplatform.key_name}"
}

output "sg_id" {
    value = "${module.security_group.this_security_group_id}"
}
output "sg_name" {
    value = "${module.security_group.this_security_group_name}"
}

output "role_id" {
    value = "${aws_iam_role.generic-role.id}"
}

output "role_name" {
    value = "${aws_iam_role.generic-role.name}"
}

output "role_arn" {
    value = "${aws_iam_role.generic-role.arn}"
}

output "status_queue_id" {
    value = "${aws_sqs_queue.status_queue.id}"
}

output "results-bucket-uri" {
    value = "${aws_s3_bucket.results-bucket.bucket_domain_name}"
}

output "results-bucket-reguri" {
    value = "${aws_s3_bucket.results-bucket.bucket_regional_domain_name}"
}