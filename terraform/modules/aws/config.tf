


/*
  Configuration YAML file
*/

locals {
    input="${var.configfile}"
}

/*
Plugin downloaded and placed in terraform/ 
*/
data "yaml_map_of_strings" "config" {
    input = "${file(local.input)}" 
}

data "yaml_map_of_strings" "config-global" {
   input = "${data.yaml_map_of_strings.config.output["global"]}" 
}
